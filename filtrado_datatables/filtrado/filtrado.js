//     var tabla1 = new Table.Build('example', {
// ajax: 'javascripts/data.json',
//     columns: [
//         {
//             name: '0',
//             filter: true,
//             filter_type: 'regexp'

//         }, {
//             name: '1',
//             filter: false,
//             filter_type: 'txt',

//         }, {
//             name: '2',
//             filter: true,
//             filter_type: 'select',
//             options: ['Tokyo', 'London', 'San Francisco', 'New York', 'Edinburgh']

//         }, {
//             name: '3',
//             filter: true,
//             filter_type: 'txt',
//             render: function (id_record, type, row, meta) {
//                 return `<p >${row[3] == 33 ? "edad 33" : "edad " + row[
// });

// Table.filter({
//     button: 'btn_filter1',
//     fieldbox: 'divfiltro1',
//     build: tabla1
// });




(function (window, document) {
    'use strict';
    var inicio = function () {
        var Date_type_columns = [], ispressed = false,

            libreria = {
                Build: function (id_table, cfg) {
                    let DT_columns = [], table, Columns_To_Filter = [], Columns_To_Filter_Type = [];
                    if (cfg) {                                                      //verifica si el usuario mandó una configuracion                         
                        var ajax = cfg.ajax;
                        DT_columns = [];

                        if (cfg.columns) {
                            cfg.columns.forEach((col, index) => {                   // recorre las columnas dadas y configura columnas para la tabla
                                if (col.render) {

                                    if (col.data.trim() == '') { alert('Campo vacio en la propiedad name de columna ' + index); return }
                                    DT_columns.push({
                                        data: col.data,
                                        render: col.render
                                    });
                                } else {

                                    if (col.data.trim() == '') { alert('Campo vacio en la propiedad name de columna ' + index); return }
                                    DT_columns.push({
                                        data: col.data,
                                    })
                                }

                                if (col.filter) {                                                  //verifica si la columna se va a filtrar y se llena las variable contenedoras de las columnas que se van a filtrar
                                    Columns_To_Filter.push(index);
                                    if (col.filter_type.toLowerCase() == 'select') {
                                        Columns_To_Filter_Type.push({ type: col.filter_type || '', options: col.options });
                                    } else if (col.filter_type.toLowerCase() == 'txt') {
                                        Columns_To_Filter_Type.push({ type: 'txt' });
                                    } else if (col.filter_type.toLowerCase() == 'regexp') {
                                        Columns_To_Filter_Type.push({ type: 'regexp' });
                                    } else if (col.filter_type.toLowerCase() == 'date') {
                                        Columns_To_Filter_Type.push({ type: 'date' });
                                    } else {
                                        alert('Defina que tipo de campo se usara en la columna ' + index); return
                                    }
                                }

                            });
                        }
                        if ($.fn.DataTable.isDataTable('#' + id_table.replace('#', ''))) {
                            $('#' + id_table.replace('#', '')).DataTable().clear().destroy();
                        }

                        if (Object.keys(DT_columns).length == 0) {
                            let drawCallback = '';
                            let columnDefs = '';
                            if (cfg.drawCallback != '' && cfg.drawCallback != null && cfg.drawCallback != 'undefined') {
                                drawCallback = cfg.drawCallback;
                            }
                            if (cfg.columnDefs != '' && cfg.columnDefs != null && cfg.columnDefs != 'undefined') {
                                columnDefs = cfg.columnDefs;
                            }

                            table = $('#' + id_table.replace('#', '')).DataTable({
                                language: {
                                    "emptyTable": "No hay datos disponibles en la tabla.",
                                    "info": "Del _START_ al _END_ de _TOTAL_ ",
                                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                                    "lengthMenu": "Mostrar _MENU_ registros",
                                    "loadingRecords": "Cargando...",
                                    "processing": "Procesando...",
                                    "search": "Buscar:",
                                    "searchPlaceholder": "Dato para buscar",
                                    "zeroRecords": "No se han encontrado coincidencias.",
                                    "paginate": {
                                        "first": "Primera",
                                        "last": "Última",
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    },
                                    "aria": {
                                        "sortAscending": "Ordenación ascendente",
                                        "sortDescending": "Ordenación descendente"
                                    }
                                },
                                // rowReorder: true,
                                responsive: true,
                                search: {
                                    regex: true
                                },
                                "ajax": ajax,
                                drawCallback: drawCallback,
                                columnDefs: columnDefs,
                                order: [[1, 'asc']],

                                initComplete: function (settings, json) {
                                    resolve();
                                }
                            });
                        } else {
                            let drawCallback = '';
                            let columnDefs = '';
                            let initComplete = '';
                            if (cfg.drawCallback != '' && cfg.drawCallback != null && cfg.drawCallback != 'undefined') {
                                drawCallback = cfg.drawCallback;
                            }
                            if (cfg.columnDefs != '' && cfg.columnDefs != null && cfg.columnDefs != 'undefined') {
                                columnDefs = cfg.columnDefs;
                            }
                            if (cfg.initComplete != '' && cfg.initComplete != null && cfg.initComplete != 'undefined') {
                                initComplete = cfg.initComplete;
                            }

                            table = $('#' + id_table.replace('#', '')).DataTable({
                                language: {
                                    "emptyTable": "No hay datos disponibles en la tabla.",
                                    "info": "Del _START_ al _END_ de _TOTAL_ ",
                                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                                    "lengthMenu": "Mostrar _MENU_ registros",
                                    "loadingRecords": "Cargando...",
                                    "processing": "Procesando...",
                                    "search": "Buscar:",
                                    "searchPlaceholder": "Dato para buscar",
                                    "zeroRecords": "No se han encontrado coincidencias.",
                                    "paginate": {
                                        "first": "Primera",
                                        "last": "Última",
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    },
                                    "aria": {
                                        "sortAscending": "Ordenación ascendente",
                                        "sortDescending": "Ordenación descendente"
                                    }
                                },
                                rowReorder: true,
                                responsive: true,
                                search: {
                                    regex: true
                                },
                                "ajax": ajax,
                                order: [[1, 'asc']],
                                drawCallback: drawCallback,
                                columnDefs: columnDefs,
                                columns: DT_columns,

                                initComplete: initComplete
                            });
                        }

                    } else {
                        let drawCallback = '';
                        let columnDefs = '';
                        if (cfg.drawCallback != '' && cfg.drawCallback != null && cfg.drawCallback != 'undefined') {
                            drawCallback = cfg.drawCallback;
                        }
                        if (cfg.columnDefs != '' && cfg.columnDefs != null && cfg.columnDefs != 'undefined') {
                            columnDefs = cfg.columnDefs;
                        }
                        table = $('#' + id_table.replace('#', '')).DataTable({
                            language: {
                                "emptyTable": "No hay datos disponibles en la tabla.",
                                "info": "Del _START_ al _END_ de _TOTAL_ ",
                                "infoEmpty": "Mostrando 0 registros de un total de 0.",
                                "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                                "lengthMenu": "Mostrar _MENU_ registros",
                                "loadingRecords": "Cargando...",
                                "processing": "Procesando...",
                                "search": "Buscar:",
                                "searchPlaceholder": "Dato para buscar",
                                "zeroRecords": "No se han encontrado coincidencias.",
                                "paginate": {
                                    "first": "Primera",
                                    "last": "Última",
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                },
                                "aria": {
                                    "sortAscending": "Ordenación ascendente",
                                    "sortDescending": "Ordenación descendente"
                                }
                            },
                            responsive: true,
                            search: {
                                regex: true
                            },
                            drawCallback: drawCallback,
                            columnDefs: columnDefs,
                            order: [[1, 'asc']],
                            // 
                            initComplete: function (settings, json) {
                                resolve();
                            }
                        });

                    }
                    //  se configura un objecto con todas las variables y se manda en la devolucion de la funcion
                    let build = { table: table, DT_columns: DT_columns, filter: { Columns_To_Filter: Columns_To_Filter, Columns_To_Filter_Type: Columns_To_Filter_Type } };

                    return build;
                },

                addrow: function (build, args) {
                    build.table.row.add(args).draw();
                },

                removerow: function (build, row_delete) {
                    build.table.row($(row_delete)).remove().draw();
                },
                clear: function (build) {
                    build.table.clear().draw();
                },

                filter: function (args) {

                    let titles = [];
                    let table_ = args.build.table;
                    let Table_ID = table_.table().node().id;
                    let firt_row = table_.row(0).data();         // se obtiene la primera fila de datos 
                    let firt_row_data = [];
                    let Header = $(`#${Table_ID} thead tr th`);     // se obtiene  la cabezera de la tabla


                    if (args.button == undefined) {
                        alert('defina ID del div donde apareceran boton de filtrado como primer parametro[GetID()]'); return
                    } if (args.fieldbox == undefined) {
                        alert('ingrese variable contenedora de datatable como segundo parametro [GetID()]'); return
                    }

                    Element = args.button;

                    if (firt_row == undefined) {
                        firt_row = $(`#${Table_ID} tbody tr td`).innerHTML;
                        if (firt_row == undefined) { firt_row = []; }
                    }

                    args.build.DT_columns.forEach(e => {           //se recorre las columnas asignadas de la devolucion de la ruta para 
                        if (firt_row[e.data] != undefined) {
                            firt_row_data.push(firt_row[e.data] || '');    //mediante esa identificador se almacena los datos de la columna en la primera fila

                        }
                    });
                    titles = [];
                    for (let i = 0; i < Header.length; i++) {   //se recorre la cabezesa de la tabla 
                        if (Header[i].innerHTML.split('<i').length == 1) {
                            titles.push(Header[i].innerHTML.replace(/\_/g, '').replace(/\[|]/g, '').replace(/ /g, '_') + Table_ID);   // se almacena titulo de cada columna
                        } else {
                            titles.push('');
                        }
                    }

                    let String_IDS = '';
                    let txtInput = '';
                    let IDS_Date_Start = [], IDS_Date_End = [];
                    let reg = new RegExp(Table_ID, "g");
                    let style = 'style="';
                    let style_campos_flex = 'style="';
                    let style_container_filters_dt = false;
                    if (!isNaN(parseFloat(args.margin))) {
                        style += `margin: ${parseFloat(args.margin)}px; `;
                    } if (args.align) {
                        if (args.align.toLowerCase() == 'left') {
                        } else if (args.align.toLowerCase() == 'right') {
                            style_campos_flex += `float: ${args.align}; `;
                        } else if (args.align.toLowerCase() == 'center') {
                            style_container_filters_dt = true;
                        }
                        console.log(args.align);
                        style_campos_flex += '"';

                    }
                    style += '"';
                    console.log(style);
                    // let margin = args.margin == undefined ? 
                    [...titles].map((title, index) => {              // se recorre los titulos equivalentes a la cantidad de columas
                        if (title != '') {
                            let index2 = args.build.filter.Columns_To_Filter.indexOf(index);
                            if (index2 != -1) {       //se verifica si esta columna esta dentro de las declaradas para filtrar, y se crea un campo segun el tipo que se le dio
                                if (args.build.filter.Columns_To_Filter_Type[index2].type.toLowerCase() == 'txt') {
                                    txtInput += `<div class="control" ${style} ><input class="form-control campo_filter" id="${title}" name="${title}" placeholder="${title.replace(/_/g, ' ').replace(reg, '')}" style="max-width: 130px" ></div>`

                                } else if (args.build.filter.Columns_To_Filter_Type[index2].type.toLowerCase() == 'select') {

                                    txtInput += `<div class="control" ${style}> <select id="${title}" class="form-control"> <option value="">${title.replace(/_/g, ' ').replace(reg, '')}</option> ${[...args.build.filter.Columns_To_Filter_Type[index2].options].map((e, index3) => {
                                        try {
                                            if (typeof e == 'string') {
                                                return `<option value="${e}">${e}</option>`;
                                            } else if (typeof e == 'object') {
                                                return `<option value="${e.value}">${e.name}</option>`;
                                            }
                                        } catch (e) {
                                            alert(e)
                                        }

                                    })}</select></div>`

                                } else if (args.build.filter.Columns_To_Filter_Type[index2].type.toLowerCase() == 'regexp') {

                                    txtInput += `<div class="control regexp" ${margin != '' ? 'style="margin:' + margin + '"' : ''}><input class="form-control campo_filter" id="${title}" name="${title}" placeholder="${title.replace(/_/g, ' ').replace(reg, '')}" style="max-width: 130px" ></div>`

                                } else if (args.build.filter.Columns_To_Filter_Type[index2].type.toLowerCase() == 'date') {
                                    IDS_Date_Start.push(`#filter_Date_Start${Table_ID + index}`);
                                    IDS_Date_End.push(`#filter_Date_End${Table_ID + index}`);
                                    txtInput += `<div id="${title}" class="simple_cont" style="display: flex; ${margin == '' ? 'margin:' + margin : ''}>
                                                            <div class="${title}" style="position: relative">
                                                                <input class="form-control campo_filter" id="filter_Date_Start${Table_ID + index}" type="text" placeholder="Fecha incial" style="max-width: 160px">
                                                            </div>
                                                            <div class="div_date_range" >
                                                                <span class="form-control"><=></span>
                                                            </div>
                                                            <div class="${title}" style="position: relative" >
                                                                <input class="form-control campo_filter" id="filter_Date_End${Table_ID + index}" type="text" placeholder="Fecha Final" style="max-width: 160px"> 
                                                            </div>
                                                        </div>`
                                }

                                String_IDS += ` #${title},`; //obtenemos los ids para añadirles un evento escucha
                            }

                        }
                    });


                    // se crea un div contenedor de los campos de filtro
                    let HTML = document.createElement('div');
                    HTML.setAttribute('id', `container_filters_dt${Table_ID}`);
                    HTML.setAttribute('class', 'container_filters_dt hide_this');
                    if (style_container_filters_dt) {
                        alert('centro')
                        HTML.style.width = 'fit-content';
                    }

                    HTML.innerHTML = `<div class="campos_flex" ${style_campos_flex}>
                                            ${txtInput}
                                     </div>`;

                    document.getElementById(Element).value = `container_filters_dt${Table_ID}`;
                    document.getElementById(args.fieldbox).appendChild(HTML);

                    String_IDS = String_IDS.slice(0, -1);                       // se elimina la ultima coma para que no ocurra algun error
                    $(`${String_IDS}`).on('keyup change', (e) => {                    // se les agrega un evento escucha      
                        let index = titles.indexOf(e.target.id);
                        if ([...e.target.parentNode.classList].indexOf('regexp') != -1) {   // verifica si es tipo regexp y ejecuta la busqueda
                            args.build.table.columns(index).search(e.target.value, true, false).draw();
                        } else {
                            args.build.table.columns(index).search(e.target.value).draw();
                        }

                    });

                    let Strings_IDS_Date_type = '';
                    IDS_Date_Start.forEach((ID_box_Date, index) => {   //aagrega DatePicker a los campos tipo fecha

                        $(ID_box_Date).datetimepicker({ format: "YYYY-MM-DD HH:mm:ss" });
                        $(IDS_Date_End[index]).datetimepicker({ format: "YYYY-MM-DD HH:mm:ss" });


                        Strings_IDS_Date_type += ID_box_Date + ', ' + IDS_Date_End[index] + ', ';  //se crea un Strin con todos los Ids de los campos tipo fecha

                    });


                    Strings_IDS_Date_type = Strings_IDS_Date_type.slice(0, -2);                 // se elimina la ultima coma para que no ocurra algun error

                    $(`${Strings_IDS_Date_type}`).on('change input dp.change', function (e) {
                        let index = titles.indexOf(e.target.parentNode.className);
                        let position = IDS_Date_Start.indexOf('#' + e.target.id);
                        if (position != -1) {
                        } else {
                            position = IDS_Date_End.indexOf('#' + e.target.id);
                        }

                        $.fn.dataTable.ext.search.push(
                            function (settings, data, dataIndex) {
                                if (settings.nTable.id != args.build.table.table().node().id) {
                                    return true;
                                }

                                let min = $(`${IDS_Date_Start[position]}`).val();
                                let max = $(`${IDS_Date_End[position]}`).val();
                                let fecha = data[index];

                                let _InitialDate = "1970-01-01 00:00:00";
                                let _FinalDate = getFormattedDate();

                                if (min) { _InitialDate = min; }
                                if (max) { _FinalDate = max; }

                                if ((_InitialDate <= fecha && fecha <= _FinalDate)) {
                                    return true;
                                }
                                return false;
                            });
                        args.build.table.draw();
                    });


                    $(`.column_to_filter${Table_ID}`).on('click', function (e) {

                        ispressed = false;
                        ispressed = e.target.attributes["aria-pressed"].nodeValue;

                        if (ispressed != 'true') {
                            document.getElementById(e.target.value).classList.remove('hide_this_dt');
                        } else {
                            let index = Date_type_columns.indexOf(e.target.value);
                            if (index != -1) {

                                document.getElementById(e.target.value).classList.add('hide_this_dt');
                                $(IDS_Date_Start[index]).val('');
                                $(IDS_Date_End[index]).val('');
                                args.build.table.draw();


                            } else {
                                document.getElementById(e.target.value).classList.add('hide_this_dt');
                                $(`#${e.target.value}`).val('');
                                let index = titles.indexOf(e.target.value);
                                args.build.table.columns(index).search('').draw();

                            }
                        }
                    });

                    var click_hide = 1;
                    $('#' + Element).on('click', function (e) {
                        if (click_hide == 1) {

                            document.getElementById(e.target.value).classList.remove('hide_this');
                            click_hide = 2;
                        } else { // se restablee filtrado 
                            let campos = document.getElementsByClassName('campo_filter');
                            [...campos].map(campo => {
                                campo.value = '';
                            });

                            args.build.table.search('').columns().search('').draw();
                            document.getElementById(e.target.value).classList.add('hide_this');
                            click_hide = 1;
                        }
                    })
                }
            }
        return libreria;
    }

    if (typeof window.libreria == undefined || 'undefined') {
        window.libreria = window.Table = inicio();
    } else {
        console.log('Se esta llamando nuevamente');
    }

})(window, document);

function getFormattedDate() {
    var date = new Date();
    var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return str;
}

function exist(elem, id) {
    let result = false;
    if ($(elem).find(id).length == 0) {
        result = true;
    } else {
        result = false;
    }
    return result;
}